let username, password, role

function login(){
	username = prompt("Please enter your username");
	password = prompt("Please enter your password");
	role = prompt("Please enter your role").toLowerCase();
}

login();

if (username === ''){
	alert("Username cannot be empty");
}
else if(password === ''){
	alert("Password cannot be empty");
}
else if(role === ''){
	alert("Role cannot be empty");
}
else{
	switch(role){
		case 'admin':
			console.log("Welcome back to the class portal, admin!");
			break;
		case 'teacher':
			console.log("Thank you for logging in, teacher!");
			break;
		case 'rookie':
			console.log("Welcome to the class portal, rookie!");
			break;
		default:
			console.log("Role out of range.")
	}
}


function letterEquivalencyOfScore(score1, score2, score3, score4){
	let avg = Math.round((score1 + score2 + score3 + score4)/4);
	console.log("The rounded off average value is: "+avg);

	if (avg <= 74){
		console.log("Hello student, your average is " + avg + ". The letter equivalent is F.");
	}
	else if (avg >= 75 && avg <= 79){
		console.log("Hello student, your average is " + avg + ". The letter equivalent is D.");
	}
	else if (avg >= 80 && avg <= 84){
		console.log("Hello student, your average is " + avg + ". The letter equivalent is C.");
	}
	else if (avg >= 85 && avg <= 89){
		console.log("Hello student, your average is " + avg + ". The letter equivalent is B.");
	}
	else if (avg >= 90 && avg <= 95){
		console.log("Hello student, your average is " + avg + ". The letter equivalent is A.");
	}
	else if (avg >= 96){
		console.log("Hello student, your average is " + avg + ". The letter equivalent is A+.");
	}
}


letterEquivalencyOfScore(90, 91, 93, 95);